PROJECT DESCRIPTION
BadinGame is just a simple game where you will run it in the terminal. It contains 3 varieties of games. The “Hangman” as stack, “Find the Carrot” as linked list and lastly, the “Snake and Ladder” as queue.

ABOUT THE AUTHORS
Programmers:
Vera Jane Lazaraga and Marjorie Anito
we are Associate in Computer Technology as a 2nd year college student in La Verdad Christian College, Apalit Pampanga. This is the first time we encounter the Python programming language, so we’re not that good in implementing these games, but with God’s help, we are trying our best to finish the project even though it’s not perfectly good.

INSTALLATION GUIDELINES
The game has been uploaded in the GitLab and to play it, you need to clone it to your computer. You just need to copy the Clone with HTML link to the GitBash.

USAGE GUIDELINES
You just need to run the code, login.py, and it will ask you if you would like to LogIn or SignUp first. 
If you choose SignUp, it will directly ask you a new username and password. If the username you entered has already matched an existed account, it will bring you back to signing up again. If you enter a password that don’t match or the password is too long, it will also bring you back to signing up again. Hence, the account has been registered and you are ready to LogIn. 
To LogIn, you just need to input your registered username and password then after, it will show the menu of what games would you like to play.

If you choose the Hangman, it will automatically let you guess what word has been inputted in the game. Careful to guess, it has limited lives, and if you already used all your lives, the game will be stopped, that means, game over!

If you choose the Snake and Ladder, it will just ask you how many players will play. Minimum players is 2 and maximum players is 4. The game is too simple to play, you just need to click Enter Key to play it and wish to win the game.

If you choose to play, Find My Carrot, It will always ask you to choose one in the two options. If you choose the correct way, it will ask you again for a two option until you succeed. Hence, the Game is over.
PERFORMANCE TESTING GUIDELINES
PERFORMANCE DOCUMENTATION
