import random
from words import words
from hangman_d import hangman_drawing
import string

def getWord(words):
    word = random.choice(words)  
    while '-' in word or ' ' in word:
        word = random.choice(words)
    return word.upper()

def hangman(): 
    word = getWord(words)
    wordLetters = set(word)  
    karaktir = set(string.ascii_uppercase)
    usedLetter = set() 
    lives = 7

    while len(wordLetters) > 0 and lives > 0:
        print('You have', lives, 'lives left and this are the letters you used: ', ' '.join(usedLetter))
        word_list = [letter if letter in usedLetter else '-' for letter in word]
        print(hangman_drawing[lives])
        print('Current word: ', ' '.join(word_list))

        user_letter = input('Guess a letter: ').upper()
        if user_letter in karaktir - usedLetter:
            usedLetter.add(user_letter)
            if user_letter in wordLetters:
                wordLetters.remove(user_letter)
                print('')

            else:
                lives = lives - 1
                print('\n', user_letter, 'is not in the word.')

        elif user_letter in usedLetter:
            print('\nYou have already used that letter. Guess another letter.')

        else:
            print('\nInvalid.')

    if lives == 0:
        print(hangman_drawing[lives])
        print('You died! The word was', word)
        ask=input("\nDo you wish to play again? [Y or N]: ")
        if ask == 'Y':
            hangman()
        elif ask == 'N':
            print("Thank you for playing")
        else:
            print("Oops, you entered a wrong choice.")
    else:
        print('Congratulations, the word', word, '!!')
        ask=input("\nDo you wish to play again? [Y or N]: ")
        if ask == 'Y':
            hangman()
        elif ask == 'N':
            print("Thank you for playing")
        else:
            print("Oops, you entered a wrong choice.")


if __name__ == '__main__':
    hangman()