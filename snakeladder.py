import random
from menu import menu

pos=[0,0,0,0]

names=[]

def diceroll():
  dice1=(random.randint(1,6))
  dice2=random.randint(1,6)
  roll=dice1+dice2
  return(roll)

ladders = {1:38, 4:10, 9:23, 21:22, 28:57, 51:16, 72:20 ,80:22}
snakes = {17:11, 54:22, 62:44, 64:14, 87:52, 93:21, 94:20, 98:21}

def nplayers():
  k=0
  while k<1:
    nplayers=int(input("How many players are playing?: "))
    if nplayers > 4:
      print("\nSorry, players should be lower than 4\n")
    elif nplayers < 2:
      print("\nPlayer should be higher than 2\n")
    else:
      k +=1
  return nplayers

num=nplayers()

def players():
  for i in range(0,num):
    print('\nPlayer', i+1, 'please input your name: ')
    name=input("")
    names.append(name)
  print('\nplayers:')
  for i in names: 
    print(i)

  print("\n - NOTE: You don't have to do anything, you just have to click Enter key and enjoy :) - \n")
  
players_pos={}
def assign():
  for i in names:
    x=names.index(i)
    players_pos[i]=pos[x]

def move():
  q=0
  while q<1:
    for i in names:
      y=diceroll()
      z=pos[names.index(i)]
      input()
      print(i,'player is currently on', z)
      input()
      print(i, 'rolled a', y)
      w=y+z
      pos[names.index(i)]=w
      print('player is now on',w)
      if pos[names.index(i)]>100:
        q+=2
      elif pos[names.index(i)]==100:
        q+=2
      if q>1:
        break
      for a in snakes:
        if a == pos[names.index(i)]:
          print("You hit a snake!")
          print("The snake is", snakes[a], "long")
          print("You go down", snakes[a], "places")
          pos[names.index(i)]-=snakes[a]
          print('you are now on', pos[names.index(i)])
      for a in ladders:
        if a ==pos[names.index(i)]:
          print("You're on a ladder!")
          print("The ladder is", ladders[a], "long")
          print("You go up", ladders[a], "places")
          pos[names.index(i)]+=ladders[a]
          print("You are now on", pos[names.index(i)])
  
def main():
  players()
  assign()
  move()
  print("\nYou won!")
  ask=input("\nDo you wish to play again? [Y or N]: ")
  if ask == 'Y':
    nplayers()
  elif ask == 'N':
    print("Thank you for playing")
  else:
    print("Oops, you entered a wrong choice.")

main()
